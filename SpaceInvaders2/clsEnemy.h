#pragma once
#include "clsobject.h"
class clsEnemy :
	public clsObject
{
public:
	clsEnemy(void);
	~clsEnemy(void);
	int getMoveSpeed(void);
	float getMoveDelay(void);

	void move(int screenWidth, int screenHeight, float x, float y);

protected:
	int moveSpeed;
	float moveDelay;

};

