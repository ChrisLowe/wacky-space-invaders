#include "StdAfx.h"
#include "clsEnemy.h"





clsEnemy::clsEnemy(void)
{
	moveSpeed = 10;
	moveDelay = 1.0f;
}


clsEnemy::~clsEnemy(void)
{
}

int clsEnemy::getMoveSpeed(void)
{
	return moveSpeed;
}

float clsEnemy::getMoveDelay(void)
{
	return moveDelay;
}

void clsEnemy::move(int screenWidth, int screenHeight, float x, float y)
{
	yPos += y;
	xPos += x;
}
