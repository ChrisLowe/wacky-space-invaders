/*
 *      SpaceInvaders.h
 *
 *      This is the workhorse header file
 *
 *
 *      CertIV Semester2 2011 C++ Assignment 1   -  Wacky space Invaders
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *      StudentID: 1013775
 *
 * 
 */

#pragma once

#include "resource.h"
#include <windows.h>
#include <windowsx.h>
#include <time.h>
#include <cstdlib> 
#include <iostream>
#include <d3d9.h>
#include <d3dx9.h>
#include <dinput.h>
#include<XAudio2.h>


// DirectX library
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")


// Screen resolution
#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600


// Keyboard hooks
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)


// Windowed mode
const bool WINDOWED_MODE = true;


// Title
const LPCWSTR TITLE = L"Wacky Space Invaders";


// DirectX prototypes
void initD3D(HWND hWnd); 
void initDInput(HINSTANCE hInstance, HWND hWnd);  
void render_frame(void); 
void detect_input(HWND hWnd);
void initSound(void);

// Game creation prototypes
void loadGame(void);
void startLevel(int level);
void createEnemies(int level);
//void loadLevel(int level);

// Game logic prototypes
void gameLoop(void);
void gameDraw(void);
void playerShoot(void);

// Cleanup prototypes
void cleanD3D(void); 
void cleanDInput(void);

// Window Message Handler prototypes
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


// The current mouse co-ordinates and mouse click masks
LONG mouseX;
LONG mouseY;
BYTE mouseClick[4];
const BYTE LEFT_MOUSE_BUTTON = 0;
const BYTE RIGHT_MOUSE_BUTTON = 1;
LONG mouseDeltaX;
LONG mouseDeltaY;

// DirectX pointers
LPDIRECT3D9 d3d;					// the pointer to our Direct3D interface
LPDIRECT3DDEVICE9 d3ddev;			// the pointer to the device class
LPD3DXSPRITE d3dspt;				// the pointer to our Direct3D Sprite interface
LPDIRECTINPUT8 din;					// the pointer to our DirectInput interface
LPDIRECTINPUTDEVICE8 dinmouse;		// the pointer to the mouse device
DIMOUSESTATE mousestate;			// the pointer to the mouse state
IXAudio2* pXAudio2;					// the pointer to the audio device