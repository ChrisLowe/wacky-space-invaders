/*
 *      SpaceInvaders.cpp
 *
 *      This is the workhorse and entry point to the game. 
 *		It handles everything from the window and object creation to game logic.
 *
 *
 *      CertIV Semester2 2011 C++ Assignment 1   -  Wacky space Invaders
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *      StudentID: 1013775
 *
 * 
 */


#include "stdafx.h"
#include "clsAbstractSprite.h"
#include "clsObject.h"
#include "clsEnemy.h"
#include "SpaceInvaders2.h"
#include "clsPlayer.h"
#include "clsBullet.h"


const int BACKGROUND_IDX = 0;
const int PLAYER_IDX = 1;



// To prove polymorphism, the player and the background and the enemy are in the same array.
clsObject *pObjects[2];


// The enemies
const int MAX_ENEMY = 80;
int enemyCount = 0;
clsObject *pEnemy[MAX_ENEMY];



// The player's bullet
int bulletCount = 0;
const int MAX_BULLET_COUNT = 8000;
clsObject * bullet[MAX_BULLET_COUNT];
const float PLAYER_SHOOT_DELAY = 1.0f;  //1.0f



// The enemy bullets
const int MAX_ENEMY_BULLET = 8000;
int enemyBulletCount = 0;
clsObject *pEnemyBullet[MAX_ENEMY_BULLET];



int level = 0;


// Load the game resources
void loadGame(void) {
	pObjects[BACKGROUND_IDX] = new clsObject();
	pObjects[BACKGROUND_IDX] = new clsObject();
	pObjects[BACKGROUND_IDX]->createPicture(d3ddev, L"background.png", 0, 0, 691, 6910);
	pObjects[BACKGROUND_IDX]->setCreateSound(pXAudio2, L"background.wav");
	//pBackground = new clsObject();
	//pBackground->createPicture(d3ddev, L"background.png", 0, 0, 691, 6910);
	//pBackground->setCreateSound(pXAudio2, L"background.wav");
	createEnemies(0);


	pObjects[PLAYER_IDX] = new clsPlayer();
	pObjects[PLAYER_IDX]->createPicture(d3ddev, L"player.png", SCREEN_WIDTH / 2 - 64, SCREEN_HEIGHT - 128 - 8, 128, 128);
	pObjects[PLAYER_IDX]->setCreateSound(pXAudio2, L"create01.wav");
	pObjects[PLAYER_IDX]->setHurtSound(pXAudio2, L"hurt01.wav");
	pObjects[PLAYER_IDX]->setDestroySound(pXAudio2, L"explode01.wav");
	pObjects[PLAYER_IDX]->setShootSound(pXAudio2, L"shoot01.wav");

	//pPlayer = new clsPlayer();
	//pPlayer->createPicture(d3ddev, L"player.png", SCREEN_WIDTH / 2 - 64, SCREEN_HEIGHT - 128 - 8, 128, 128);

	//pPlayer->setCreateSound(pXAudio2, L"create01.wav");
	//pPlayer->setHurtSound(pXAudio2, L"hurt01.wav");
	//pPlayer->setDestroySound(pXAudio2, L"explode01.wav");
	//pPlayer->setShootSound(pXAudio2, L"shoot01.wav");



}

void createEnemies(int level)
{
	bool isCreating = true;
	int startX = 150;
	int startY = 62;
	int endX = 650;
	int endY = 262;
	int xOff = 9;
	int yOff = 9;
	int xInc = 50;
	int yInc = 50;
	int currX = startX;
	int currY = startY;

	//Clear the array
	for (int i = 0; i < MAX_ENEMY; i++)
		pEnemy[i] = NULL;

	enemyCount = 0;

	while ((isCreating == true) && (enemyCount < MAX_ENEMY))
	{
		pEnemy[enemyCount] = new clsEnemy();
		pEnemy[enemyCount]->createPicture(d3ddev, L"enemy01.png", (float) currX + xOff, (float) currY + yOff, 32, 32);
		pEnemy[enemyCount]->setCreateSound(pXAudio2, L"create01.wav");
		pEnemy[enemyCount]->setHurtSound(pXAudio2, L"hurt01.wav");
		pEnemy[enemyCount]->setDestroySound(pXAudio2, L"explode01.wav");
		pEnemy[enemyCount]->setShootSound(pXAudio2, L"shoot01.wav");

		currX += xInc;
		
		if (currX > endX) {
			currX = startX;
			currY += yInc;
		}

		if (currY > endY) {
			isCreating = false;
		}

		enemyCount++;

	}

}



// Draw the objects in the game
void gameDraw(void) {
	pObjects[BACKGROUND_IDX]->drawPicture(d3dspt);

	//Draw the player and check if they have been hit
	pObjects[PLAYER_IDX];
	pObjects[PLAYER_IDX]->drawPicture(d3dspt);

	pObjects[PLAYER_IDX]->collision(pEnemyBullet, enemyBulletCount, pXAudio2);



	//Draw the enemies and check if they have been hit.
	for (int i = 0; i < enemyCount; i++) {
		pEnemy[i]->drawPicture(d3dspt);
		pEnemy[i]->collision(bullet, bulletCount, pXAudio2);

	}

	

	//Occasionally, make the enemy shoot a bullet
	static float enemyShootDelay = 0.0f;
	enemyShootDelay += 0.1f;


	//Select a random, valid enemy
	srand( time(NULL) );
	int random = (rand() % MAX_ENEMY);

	if ((enemyShootDelay > 5.0f) && (pEnemy[random] != NULL) && (pEnemy[random]->isDead() == false)) {
		enemyShootDelay = 0.0f;

		int shootX = pEnemy[random]->getXPos();
		int shootY = pEnemy[random]->getYPos();
		pEnemyBullet[enemyBulletCount] = new clsObject();
		pEnemyBullet[enemyBulletCount]->createPicture(d3ddev, L"bullet02.png", shootX, shootY, 16, 16);
		enemyBulletCount++;
	}

	//Draw the enemy bullets
	for (int i = 0; i < enemyBulletCount; i++) {
		if (pEnemyBullet[i] != NULL) 
			pEnemyBullet[i]->drawPicture(d3dspt);
	}


	//Draw the players bullets. I would prefer dynamic list but this is easier
	for (int i = 0; i < bulletCount; i++) {

		if (bullet[i] != NULL) {
			bullet[i]->drawPicture(d3dspt);
			bullet[i]->changeLocation(bullet[i]->getXPos(), bullet[i]->getYPos() - 10);

			//Delete bullets that go off screen
			if (bullet[i]->getYPos() < 0) {
				delete bullet[i];
				bullet[i] = 0;
			}
		}
	}


	//Draw the enemy bullets
	for (int i = 0; i < enemyBulletCount; i++) {
		if (pEnemyBullet[i] != NULL) {
			pEnemyBullet[i]->drawPicture(d3dspt);
			pEnemyBullet[i]->changeLocation(pEnemyBullet[i]->getXPos(), pEnemyBullet[i]->getYPos() + 10);
		}

	}

}

int enemyDirectionMod = 1;



// Move the objects in the games
void gameLoop(void) {
	
	pObjects[BACKGROUND_IDX]->move(SCREEN_WIDTH, SCREEN_HEIGHT, 0, -4);

	pObjects[PLAYER_IDX]->changeLocation(mouseX, SCREEN_HEIGHT - 128);



	//Move the enemies
	static float enemyMoveDelay = 0.0f;
	enemyMoveDelay += 0.1f;
	float maxX = 0.0f;
	float minX = 9999.9f;
	int activeEnemies = 0;

	if (enemyMoveDelay > 1.0 - (level * 0.1)) {
		enemyMoveDelay = 0.0;

		for (int i = 0; i < enemyCount; i++) {
			pEnemy[i]->move(SCREEN_WIDTH, SCREEN_HEIGHT, 10.0f * enemyDirectionMod * (0.25 * level + 1), 0.0f);
			float x = pEnemy[i]->getXPos();

			// Keep track of the maximum and minimum x of the enemies
			if (pEnemy[i]->isDead() == false) {
				if (x >= maxX) 
					maxX = x;

				if (x <= minX)
					minX = x;

				activeEnemies++;
				
			}
		}

		// No more enemies left.  Wait for a while then recreate the level
		if (activeEnemies == 0) {
			level++;
			createEnemies(level);
		}


		int moveDownSize = 10;

		if ((maxX >= SCREEN_WIDTH - 32) || (minX <= 0)) {
			enemyDirectionMod *= -1;
			for (int i = 0; i < enemyCount; i++) {
				pEnemy[i]->changeLocation(pEnemy[i]->getXPos(), pEnemy[i]->getYPos() + moveDownSize);
			}
		}
	}
}


// Right mouse button action
void playerShoot(void) {

		//Apply a delay to the weapon
	static float shootDelay = 0.0f;

	//Start at the muzzel of the ship image. Took some pixel pushing to get this right
	float shootX = pObjects[PLAYER_IDX]->getXPos() + 53;
	float shootY = pObjects[PLAYER_IDX]->getYPos();
	shootDelay += 1;

	if (shootDelay > PLAYER_SHOOT_DELAY) {
		shootDelay = 0.0f;  //shot fired .. reset delay

		pObjects[PLAYER_IDX]->playShootSound(pXAudio2);


		if (bulletCount < MAX_BULLET_COUNT) {
			bullet[bulletCount] = new clsObject();
			bullet[bulletCount]->createPicture(d3ddev, L"bullet01.png", shootX, shootY, 16, 16);
			bullet[bulletCount]->changeLocation(shootX, shootY);
			bulletCount++; //<-- this looks dangerous to me
		}
	} 
}



// The Entry point
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
	// Create and register a window
    HWND hWnd;
    WNDCLASSEX wc;

    ZeroMemory(&wc, sizeof(WNDCLASSEX));
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = (WNDPROC)WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.lpszClassName = L"WindowClass1";

    RegisterClassEx(&wc);

    hWnd = CreateWindowEx(NULL,
                          L"WindowClass1",
                          TITLE,
                          WS_OVERLAPPEDWINDOW,
                          0, 0,
                          SCREEN_WIDTH, SCREEN_HEIGHT,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hWnd, nCmdShow);

    // Initialize Direct3D
    initD3D(hWnd);
	
	// Initialize DirectInput
	initDInput(hInstance, hWnd);    

	// Initialize Sound
	initSound();

	// Play the intro sound
	pObjects[BACKGROUND_IDX]->playCreateSound(pXAudio2);


    // Main loop
    MSG msg;
    while(TRUE)
    {
        DWORD starting_point = GetTickCount();

        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                break;

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

		detect_input(hWnd);
        render_frame();

		// Game loop
		gameLoop();

        // Window exit condition ('escape' key)
        if(KEY_DOWN(VK_ESCAPE))
            PostMessage(hWnd, WM_DESTROY, 0, 0);

		// Limit framerate
        while ((GetTickCount() - starting_point) < 25);
    }


    // clean up DirectX and COM
    cleanD3D();
	cleanDInput();

    return msg.wParam;
}



// Initializes DirectInput
void initDInput(HINSTANCE hInstance, HWND hWnd)
{
    // Create the DirectInput interface
    DirectInput8Create(hInstance,    
                       DIRECTINPUT_VERSION,    
                       IID_IDirectInput8,  
                       (void**)&din,    
                       NULL); 

	// Create the mouse device interface
    din->CreateDevice(GUID_SysMouse,
                      &dinmouse,
                      NULL);

    // Set the mouse interface format
    dinmouse->SetDataFormat(&c_dfDIMouse);

    // Set the mouse interface control
    dinmouse->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND);
}



// Updates the latest mouse input events
void detect_input(HWND hWnd)
{

	// Create storage for the mouse-states
	static DIMOUSESTATE mousestate;    
    dinmouse->Acquire();

	// Mouse click state
	mouseClick[0] = mousestate.rgbButtons[0];
	mouseClick[1] = mousestate.rgbButtons[1];
	mouseClick[2] = mousestate.rgbButtons[2];
	mouseClick[3] = mousestate.rgbButtons[3];

	// Mouse delta
	mouseDeltaX = mousestate.lX;
	mouseDeltaY = mousestate.lY;

	// Relative mouse position
    POINT cursorPos;
    GetCursorPos(&cursorPos);
    mouseX = cursorPos.x;
    mouseY = cursorPos.y;
	

	// Right Mouse Button .. Fire!
	if (mouseClick[0] & 0x80) {
		playerShoot();
	}

    dinmouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mousestate);
}




// Initializes Direct3D for use
void initD3D(HWND hWnd)
{
	// I don't understand any of this...
    d3d = Direct3DCreate9(D3D_SDK_VERSION);
    D3DPRESENT_PARAMETERS d3dpp;

    ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = WINDOWED_MODE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.hDeviceWindow = hWnd;
    d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
    d3dpp.BackBufferWidth = SCREEN_WIDTH;
    d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	
	d3d->CreateDevice(D3DADAPTER_DEFAULT,
                      D3DDEVTYPE_HAL,
                      hWnd,
                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                      &d3dpp,
                      &d3ddev);

    D3DXCreateSprite(d3ddev, &d3dspt);

	// Load game
	loadGame();

    return;
}

// Initializes the XAudio2 device
void initSound()
{
	CoInitializeEx( NULL, COINIT_MULTITHREADED );
	pXAudio2 = NULL;
	XAudio2Create( &pXAudio2, 0, XAUDIO2_DEFAULT_PROCESSOR );
	IXAudio2MasteringVoice* pMasterVoice = NULL;

	pXAudio2->CreateMasteringVoice( &pMasterVoice, XAUDIO2_DEFAULT_CHANNELS,
                            XAUDIO2_DEFAULT_SAMPLERATE, 0, 0, NULL );
}



// Render a single frame
void render_frame(void)
{
    //Begin the frame
    d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
    d3ddev->BeginScene(); 
    d3dspt->Begin(D3DXSPRITE_ALPHABLEND);   

	// Draw the game
	gameDraw();

	//Clean up the frame
    d3dspt->End();    
    d3ddev->EndScene();  
    d3ddev->Present(NULL, NULL, NULL, NULL);
}




// Clean Direct3D
void cleanD3D(void)
{
    d3ddev->Release();
    d3d->Release();
	dinmouse->Unacquire();
}

// Clean DirectInput
void cleanDInput(void)
{
    dinmouse->Unacquire();    // make sure the mouse in unacquired
    din->Release();    // close DirectInput before exiting
}


// Window Message Handler
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_DESTROY:
            {
                PostQuitMessage(0);
                return 0;
            } break;
    }

    return DefWindowProc (hWnd, message, wParam, lParam);
}

