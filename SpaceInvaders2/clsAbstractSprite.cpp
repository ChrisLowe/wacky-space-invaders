/*
 *      clsAbstractSprite.cpp
 *
 *      Source file for the abstract sprite.
 *
 *			- Holds the xPos & yPos, and the pictures width and height
 *
 *
 *      As part of CertIV Semester2 2011 Assignment 1   -  Wacky space Invaders
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *      StudentID: 1013775
 *
 * 
 */

#include "StdAfx.h"
#include "clsAbstractSprite.h"


clsAbstractSprite::clsAbstractSprite(void)
{
}


clsAbstractSprite::~clsAbstractSprite(void)
{
	sprite->Release();
}

void clsAbstractSprite::changeLocation(float newX, float newY) {
	xPos = newX;
	yPos = newY;
}

float clsAbstractSprite::getXPos() {
	return xPos;
}

float clsAbstractSprite::getYPos() {
	return yPos;
}

int clsAbstractSprite::getHeight() {
	return height;
}

int clsAbstractSprite::getWidth() {
	return width;
}


// Create the sprite
void clsAbstractSprite::createPicture(LPDIRECT3DDEVICE9 dev, LPWSTR filename, float startx, float starty, int mwidth, int mheight) {

	xPos = startx;
	yPos = starty;
	width = mwidth;
	height = mheight;

	//Transparency
	D3DXCreateTextureFromFileEx(dev,    // the device pointer
                             filename,    // the file name
                             D3DX_DEFAULT,    // default width
                             D3DX_DEFAULT,    // default height
                             D3DX_DEFAULT,    // no mip mapping
                             NULL,				// regular usage
                             D3DFMT_A8R8G8B8,    // 32-bit pixels with alpha
                             D3DPOOL_MANAGED,    // typical memory handling
                             D3DX_DEFAULT,    // no filtering
                             D3DX_DEFAULT,    // no mip filtering
                             D3DCOLOR_XRGB(255, 0, 255),		// the transparent colour key  - Pink ~~
                             NULL,    // no image info struct
                             NULL,    // not using 256 colors
                             &sprite);    // load to sprite
}


// Draw the sprite
void clsAbstractSprite::drawPicture(LPD3DXSPRITE spt) 
{
	D3DXVECTOR3 position(xPos, yPos, 0);
	spt->Draw(sprite, NULL, NULL, &position, D3DCOLOR_ARGB(255, 255, 255, 255));
}