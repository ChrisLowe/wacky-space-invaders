#pragma once
#include "clsabstractsprite.h"


class clsObject :
	public clsAbstractSprite
{
public:
	clsObject(void);
	~clsObject(void);

	virtual void move(int screenWidth, int screenHeight, float x, float y);
	
	
	bool isCollision(void);
	virtual void collision(clsObject *objects[], int numberOfObjects, IXAudio2* pXA2);
	void setDead(void);
	bool isDead(void);
	void damage(float amount, IXAudio2* pXA2);
	void deflect(void);
	float getHealth(void);
	
	void setCreateSound(IXAudio2 *pXAudio2, LPWSTR filename);
	void setHurtSound(IXAudio2 *pXAudio2, LPWSTR filename);
	void setDestroySound(IXAudio2 *pXAudio2, LPWSTR filename);
	void setShootSound(IXAudio2 *pXAudio2, LPWSTR filename);

	void playCreateSound(IXAudio2 *pXAudio2);
	void playHurtSound(IXAudio2 *pXAudio2);
	void playDestroySound(IXAudio2 *pXAudio2);
	void playShootSound(IXAudio2 *pXAudio2);


protected:
	bool isCollide;
	bool dead;
	float health;

	IXAudio2 *pXA2;
	WAVEFORMATEXTENSIBLE wfx;

	//XAUDIO2_BUFFER buffer;
	XAUDIO2_BUFFER createBuffer;
	XAUDIO2_BUFFER hurtBuffer;
	XAUDIO2_BUFFER destroyBuffer;
	XAUDIO2_BUFFER shootBuffer;


	IXAudio2SourceVoice* pSourceVoice;
	//IXAudio2SourceVoice* pHurtVoice;
	//IXAudio2SourceVoice* pDestroyVoice;
	//IXAudio2SourceVoice* pShootVoice;

};

