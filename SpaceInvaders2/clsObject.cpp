#include "StdAfx.h"
#include "clsObject.h"


const int collisionDamage = 28;
const int partialCollisionDamage = 16;

clsObject::clsObject(void)
{
	health = 100;
	dead = false;
	isCollide = false;
}


clsObject::~clsObject(void)
{
}

void clsObject::move(int screenWidth, int screenHeight, float x, float y)
{

	xPos += x;
	yPos += y;

}


void clsObject::collision(clsObject *objects[], int numberOfObjects, IXAudio2* pXA2)
{
	
	isCollide = false;

	for (int i = 0; i < numberOfObjects; i++) 
	{
		if ((objects[i] != this) && (objects[i] != NULL))
		{

			int oHeight = objects[i]->getHeight();
			int oWidth = objects[i]->getWidth();
			float oX = objects[i]->getXPos();
			float oY = objects[i]->getYPos();

			// Direct hit collision
			// Check if the object is inside this object
			if ((oY <= yPos + height) && (oY >= yPos) &&
				(oX <= xPos + width) && (oX >= xPos)) {
				isCollide = true;

				//Destroy the projectile (source object) on a direct hit
				objects[i]->setDead();
				delete objects[i];
				objects[i] = 0;  //BUG FIX - had to delete then set to 0. Not sure why this works 

				//Apply damage
				damage(collisionDamage, pXA2);
			}
				
			
			// Partial hit
			// Check if the corners collide
			if (
				//Top left corner
				((xPos >= oX) && (xPos <= oX + oWidth) &&
				(yPos >= oY) && (yPos <= oY + oHeight)) ||

				//Top right corner
				((xPos + width >= oX) && (xPos + width <= oX + oWidth) &&
				(yPos >= oY) && (yPos <= oY + oHeight)) ||

				// Bottom left corner
				((xPos >= oX) && (xPos <= oX + oWidth) &&
				(yPos + height >= oY) && (yPos + height <= oY + oHeight)) ||

				// Bottom right corner
				((xPos + width >= oX) && (xPos + width <= oX + oWidth) &&
				(yPos + height >= oY) && (yPos + height <= oY + oHeight)))

			{
				isCollide = true;

				//Do not destroy projectile and add a little less damage
				damage(partialCollisionDamage, pXA2);
			}
		}
	}
}


void clsObject::damage(float amount, IXAudio2* pXA2) {
	health -= amount;

	if (health < 0) {
		playDestroySound(pXA2);
		setDead();
	} else {
		//playHurtSound(pXA2);
	}
}

float clsObject::getHealth(void) {
	return health;
}

bool clsObject::isDead(void) {
	return dead;
}

void clsObject::setDead(void) {
	dead = true;
	xPos = -100;  //move it off view .. only here because dead, undrawn objects were causing collisions
	yPos = -100;
}

bool clsObject::isCollision(void) {
	return isCollide;
}

void clsObject::setCreateSound(IXAudio2 *pXAudio2, LPWSTR filename)
{
	//IXAudio2SourceVoice* pSourceVoice = NULL;
	createSound(pXAudio2, &pSourceVoice, filename, &createBuffer, &wfx);
}

void clsObject::setHurtSound(IXAudio2 *pXAudio2, LPWSTR filename)
{
	//IXAudio2SourceVoice* pSourceVoice = NULL;
	createSound(pXAudio2, &pSourceVoice, filename, &hurtBuffer, &wfx);
}

void clsObject::setDestroySound(IXAudio2 *pXAudio2, LPWSTR filename)
{
	//IXAudio2SourceVoice* pSourceVoice = NULL;
	createSound(pXAudio2, &pSourceVoice, filename, &destroyBuffer, &wfx);
}

void clsObject::setShootSound(IXAudio2 *pXAudio2, LPWSTR filename)
{
	//IXAudio2SourceVoice* pSourceVoice = NULL;
	createSound(pXAudio2, &pSourceVoice, filename, &shootBuffer, &wfx);
}


void clsObject::playCreateSound(IXAudio2 *pXAudio2)
{
	pXAudio2->CreateSourceVoice( &pSourceVoice, (WAVEFORMATEX*)&wfx,
				  0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL );
	pSourceVoice->SubmitSourceBuffer( &createBuffer );
	pSourceVoice->Start( 0, XAUDIO2_COMMIT_NOW );

}

void clsObject::playHurtSound(IXAudio2 *pXAudio2)
{
	pXAudio2->CreateSourceVoice( &pSourceVoice, (WAVEFORMATEX*)&wfx,
				  0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL );
	pSourceVoice->SubmitSourceBuffer( &hurtBuffer );
	pSourceVoice->Start( 0, XAUDIO2_COMMIT_NOW );

}

void clsObject::playDestroySound(IXAudio2 *pXAudio2)
		{
	pXAudio2->CreateSourceVoice( &pSourceVoice, (WAVEFORMATEX*)&wfx,
				  0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL );
	pSourceVoice->SubmitSourceBuffer( &destroyBuffer );
	pSourceVoice->Start( 0, XAUDIO2_COMMIT_NOW );

}

void clsObject::playShootSound(IXAudio2 *pXAudio2)
{
	pXAudio2->CreateSourceVoice( &pSourceVoice, (WAVEFORMATEX*)&wfx,
				  0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL );
	pSourceVoice->SubmitSourceBuffer( &shootBuffer );
	pSourceVoice->Start( 0, XAUDIO2_COMMIT_NOW );

}