/*
 *      clsAbstractSprite.h
 *
 *      Header file for the abstract sprite.
 *		Holds the xPos, yPox, width and height and implements the getters/setters for these
 *
 *
 *      As part of CertIV Semester2 2011 Assignment 1   -  Wacky space Invaders
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *      StudentID: 1013775
 *
 * 
 */


#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <XAudio2.h>
#include "playSound.h"


// The Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

class clsAbstractSprite
{
public:
	clsAbstractSprite(void);
	~clsAbstractSprite(void);

	virtual void move(int screenWidth, int screenHeight, float x, float y)= 0;
	
	void changeLocation(float newX, float newY);
	float getXPos();
	float getYPos();
	int getWidth();
	int getHeight();

	virtual void createPicture(LPDIRECT3DDEVICE9 dev, LPWSTR filename, float startXPos, float startYPos, int mwidth, int mheight);
	virtual void drawPicture(LPD3DXSPRITE spt);


	//Sounds
	virtual void setCreateSound(IXAudio2 *pXAudio2, LPWSTR filename)= 0;
	virtual void setHurtSound(IXAudio2 *pXAudio2, LPWSTR filename)= 0;
	virtual void setDestroySound(IXAudio2 *pXAudio2, LPWSTR filename)= 0;
	virtual void setShootSound(IXAudio2 *pXAudio2, LPWSTR filename)= 0;

	virtual void playCreateSound(IXAudio2 *pXAudio2)= 0;
	virtual void playHurtSound(IXAudio2 *pXAudio2)= 0;
	virtual void playDestroySound(IXAudio2 *pXAudio2)= 0;
	virtual void playShootSound(IXAudio2 *pXAudio2)= 0;




protected:
	LPDIRECT3DTEXTURE9 sprite;		// the pointer to the sprite
	int width, height;
	float xPos, yPos;
};
